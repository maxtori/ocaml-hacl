all: build

build:
	dune build @install

dev:
	dune build

clean:
	dune clean
