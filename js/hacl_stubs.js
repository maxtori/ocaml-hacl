//Provides: ml_randombytes
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu64
function ml_randombytes(buf) {
  wasm_ready();
  var len = wu64(buf.data.length);
  var buf_ptr = walloc_u8(wasm, buf);
  wasm._randombytes(buf_ptr, len);
  wextract_u8(wasm, buf, buf_ptr);
  return 0
}

//Provides: ml_hacl_HMAC_SHA2_256_hmac
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu32
function ml_hacl_HMAC_SHA2_256_hmac(mac, key, data) {
  wasm_ready();
  var keylen = wu32(key.data.length);
  var datalen = wu32(data.data.length);
  var mac_ptr = walloc_u8(wasm, mac);
  var key_ptr = walloc_u8(wasm, key);
  var data_ptr = walloc_u8(wasm, data);
  wasm._Hacl_HMAC_SHA2_256_hmac(mac_ptr, key_ptr, keylen, data_ptr, datalen)
  wextract_u8(wasm, mac, mac_ptr);
  return 0
}

//Provides: ml_hacl_SHA2_256_init
//Requires: wasm, wasm_ready, walloc_u32, wextract_u32
function ml_hacl_SHA2_256_init(state) {
  wasm_ready();
  var state_ptr = walloc_u32(wasm, state);
  wasm._Hacl_SHA2_256_init(state_ptr);
  wextract_u32(wasm, state, state_ptr);
  return 0
}

//Provides: ml_hacl_SHA2_256_update
//Requires: wasm, wasm_ready, walloc_u32, wextract_u32, walloc_u8, wextract_u8
function ml_hacl_SHA2_256_update(state, data) {
  wasm_ready();
  var state_ptr = walloc_u32(wasm, state);
  var data_ptr = walloc_u8(wasm, data);
  wasm._Hacl_SHA2_256_update(state_ptr, data_ptr);
  wextract_u32(wasm, state, state_ptr);
  return 0
}

//Provides: ml_hacl_SHA2_256_update_last
//Requires: wasm, wasm_ready, walloc_u32, wextract_u32, walloc_u8, wextract_u8, wu32
function ml_hacl_SHA2_256_update_last(state, data, datalen) {
  wasm_ready();
  var datalen = wu32(datalen);
  var state_ptr = walloc_u32(wasm, state);
  var data_ptr = walloc_u8(wasm, data);
  wasm._Hacl_SHA2_256_update_last(state_ptr, data_ptr, datalen);
  wextract_u32(wasm, state, state_ptr);
  return 0
}

//Provides: ml_hacl_SHA2_256_finish
//Requires: wasm, wasm_ready, walloc_u32, wextract_u32, walloc_u8, wextract_u8
function ml_hacl_SHA2_256_finish(state, hash) {
  wasm_ready();
  var state_ptr = walloc_u32(wasm, state);
  var hash_ptr = walloc_u8(wasm, hash);
  wasm._Hacl_SHA2_256_finish(state_ptr, hash_ptr);
  wextract_u32(wasm, state, state_ptr);
  wextract_u8(wasm, hash, hash_ptr);
  return 0
}

//Provides: ml_hacl_SHA2_512_init
//Requires: wasm, wasm_ready, walloc_u32, wextract_u32
function ml_hacl_SHA2_512_init(state) {
  wasm_ready();
  var state_ptr = walloc_u32(wasm, state);
  wasm._Hacl_SHA2_512_init(state_ptr);
  wextract_u32(wasm, state, state_ptr);
  return 0
}

//Provides: ml_hacl_SHA2_512_update
//Requires: wasm, wasm_ready, walloc_u32, wextract_u32, walloc_u8, wextract_u8
function ml_hacl_SHA2_512_update(state, data) {
  wasm_ready();
  var state_ptr = walloc_u32(wasm, state);
  var data_ptr = walloc_u8(wasm, data);
  wasm._Hacl_SHA2_512_update(state_ptr, data_ptr);
  wextract_u32(wasm, state, state_ptr);
  return 0
}

//Provides: ml_hacl_SHA2_512_update_last
//Requires: wasm, wasm_ready, walloc_u32, wextract_u32, walloc_u8, wextract_u8, wu32
function ml_hacl_SHA2_512_update_last(state, data, datalen) {
  wasm_ready();
  var datalen = wu32(datalen);
  var state_ptr = walloc_u32(wasm, state);
  var data_ptr = walloc_u8(wasm, data);
  wasm._Hacl_SHA2_512_update_last(state_ptr, data_ptr, datalen);
  wextract_u32(wasm, state, state_ptr);
  return 0
}

//Provides: ml_hacl_SHA2_512_finish
//Requires: wasm, wasm_ready, walloc_u32, wextract_u32, walloc_u8, wextract_u8
function ml_hacl_SHA2_512_finish(state, hash) {
  wasm_ready();
  var state_ptr = walloc_u32(wasm, state);
  var hash_ptr = walloc_u8(wasm, hash);
  wasm._Hacl_SHA2_512_finish(state_ptr, hash_ptr);
  wextract_u32(wasm, state, state_ptr);
  wextract_u8(wasm, hash, hash_ptr);
  return 0
}

//Provides: ml_hacl_Curve25519_crypto_scalarmult
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8
function ml_hacl_Curve25519_crypto_scalarmult(pk, sk, bp) {
  wasm_ready();
  var pk_ptr = walloc_u8(wasm, pk);
  var sk_ptr = walloc_u8(wasm, sk);
  var bp_ptr = walloc_u8(wasm, bp);
  wasm._Hacl_Curve25519_crypto_scalarmult(pk_ptr, sk_ptr, bp_ptr);
  wextract_u8(wasm, pk, pk_ptr);
  return 0
}

//Provides: ml_nacl_crypto_secretbox_easy
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu64
function ml_nacl_crypto_secretbox_easy(c, m, n, k) {
  wasm_ready();
  var len = wu64(m.data.length - 32);
  var c_ptr = walloc_u8(wasm, c);
  var m_ptr = walloc_u8(wasm, m);
  var n_ptr = walloc_u8(wasm, n);
  var k_ptr = walloc_u8(wasm, k);
  wasm._NaCl_crypto_secretbox_easy(c_ptr, m_ptr, len, n_ptr, k_ptr);
  wextract_u8(wasm, c, c_ptr);
  return 0
}

//Provides: ml_nacl_crypto_secretbox_open_detached
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu64
function ml_nacl_crypto_secretbox_open_detached(m, c, mac, n, k) {
  wasm_ready();
  var len = wu64(c.data.length - 32);
  var m_ptr = walloc_u8(wasm, m);
  var c_ptr = walloc_u8(wasm, c);
  var mac_ptr = walloc_u8(wasm, mac);
  var n_ptr = walloc_u8(wasm, n);
  var k_ptr = walloc_u8(wasm, k);
  var r = wasm._NaCl_crypto_secretbox_open_detached(m_ptr, c_ptr, mac_ptr, len, n_ptr, k_ptr);
  wextract_u8(wasm, m, m_ptr);
  return r;
}

//Provides: ml_nacl_crypto_box_beforenm
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8
function ml_nacl_crypto_box_beforenm(k, pk, sk) {
  wasm_ready();
  var k_ptr = walloc_u8(wasm, k);
  var pk_ptr = walloc_u8(wasm, pk);
  var sk_ptr = walloc_u8(wasm, sk);
  wasm._NaCl_crypto_box_beforenm(k_ptr, pk_ptr, sk_ptr);
  wextract_u8(wasm, k, k_ptr);
  return 0;
}

//Provides: ml_nacl_crypto_box_easy_afternm
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu64
function ml_nacl_crypto_box_easy_afternm(c, m, n, k) {
  wasm_ready();
  var len = wu64(m.data.length - 32);
  var c_ptr = walloc_u8(wasm, c);
  var m_ptr = walloc_u8(wasm, m);
  var n_ptr = walloc_u8(wasm, n);
  var k_ptr = walloc_u8(wasm, k);
  wasm._NaCl_crypto_box_afternm(c_ptr, m_ptr, len, n_ptr, k_ptr);
  wextract_u8(wasm, c, c_ptr);
  return 0;
}

//Provides: ml_nacl_crypto_box_open_easy_afternm
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu64
function ml_nacl_crypto_box_open_easy_afternm(m, c, n, k) {
  wasm_ready();
  var len = wu64(c.data.length - 32);
  var m_ptr = walloc_u8(wasm, m);
  var c_ptr = walloc_u8(wasm, c);
  var n_ptr = walloc_u8(wasm, n);
  var k_ptr = walloc_u8(wasm, k);
  wasm._NaCl_crypto_box_afternm(m_ptr, c_ptr, len, n_ptr, k_ptr);
  wextract_u8(wasm, m, m_ptr);
  return 0;
}

//Provides: ml_hacl_Ed25519_secret_to_public
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8
function ml_hacl_Ed25519_secret_to_public(pk, sk) {
  wasm_ready();
  var pk_ptr = walloc_u8(wasm, pk);
  var sk_ptr = walloc_u8(wasm, sk);
  wasm._Hacl_Ed25519_secret_to_public(pk_ptr, sk_ptr);
  wextract_u8(wasm, pk, pk_ptr);
  return 0
}

//Provides: ml_hacl_Ed25519_secret_expanded_to_public
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8
function ml_hacl_Ed25519_secret_expanded_to_public(pk, sk) {
  wasm_ready();
  var pk_ptr = walloc_u8(wasm, pk);
  var sk_ptr = walloc_u8(wasm, sk);
  wasm._Hacl_Ed25519_secret_expanded_to_public(pk_ptr, sk_ptr);
  wextract_u8(wasm, pk, pk_ptr);
  return 0
}

//Provides: ml_hacl_Ed25519_sign
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu32
function ml_hacl_Ed25519_sign(sig, sk, m) {
  wasm_ready();
  var len = wu32(m.data.length);
  var sig_ptr = walloc_u8(wasm, sig);
  var sk_ptr = walloc_u8(wasm, sk);
  var m_ptr = walloc_u8(wasm, m);
  wasm._Hacl_Ed25519_sign(sig_ptr, sk_ptr, m_ptr, len);
  wextract_u8(wasm, sig, sig_ptr);
  return 0
}

//Provides: ml_hacl_Ed25519_sign_expanded
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu32
function ml_hacl_Ed25519_sign_expanded(sig, sk, m) {
  wasm_ready();
  var len = wu32(m.data.length);
  var sig_ptr = walloc_u8(wasm, sig);
  var sk_ptr = walloc_u8(wasm, sk);
  var m_ptr = walloc_u8(wasm, m);
  wasm._Hacl_Ed25519_sign_expanded(sig_ptr, sk_ptr, m_ptr, len);
  wextract_u8(wasm, sig, sig_ptr);
  return 0
}


//Provides: ml_hacl_Ed25519_verify
//Requires: wasm, wasm_ready, walloc_u8, wextract_u8, wu32
function ml_hacl_Ed25519_verify(pk, m, sig) {
  wasm_ready();
  var len = wu32(m.data.length);
  var pk_ptr = walloc_u8(wasm, pk);
  var m_ptr = walloc_u8(wasm, m);
  var sig_ptr = walloc_u8(wasm, sig);
  return wasm._Hacl_Ed25519_verify(pk_ptr, m_ptr, len, sig_ptr)
}
